<?php
class fs_scanner
{
    /**
     * Path to scan
     * @var string
     */
    protected $_path;

    protected $_validMusicExt = [
        'mp3'
    ];


    /**
     * fs_scanner constructor.
     * @param string $pathToMedia path to scan
     *
     * @throws InvalidArgumentException when path not acceptable
     */
    public function __construct(string $pathToMedia)
    {
        if(!isset($pathToMedia) || !is_dir($pathToMedia)){
            throw new InvalidArgumentException("{$pathToMedia} not acceptable!\n");
        }

        $this->_path = $pathToMedia;
        if(!is_dir($this->_path . DIRECTORY_SEPARATOR . 'invalid')){
            mkdir($this->_path . DIRECTORY_SEPARATOR . 'invalid', 0777, true);
        }
    }

    public function run()
    {
        $music = $this->_getMusicFiles($this->_path);
        echo "\n\n\n\nStart Move\n\n\n\n";
        /** @var file_iface $file */
        foreach ($music as $from => $file){
            echo "moving {$from}\n";
            try {
                if(!is_dir($this->_path . $file->getDir())){
                    mkdir($this->_path . $file->getDir(), 0777, true);
                }
                $this->_copy($from, $this->_path . DIRECTORY_SEPARATOR . $file->getDir() . $file->getFileName(pathinfo($from, PATHINFO_EXTENSION)));
            } catch (Exception $e) {
                $this->_copy($from,  $this->_path . DIRECTORY_SEPARATOR . 'invalid' . DIRECTORY_SEPARATOR . pathinfo($from, PATHINFO_BASENAME));
            }
        }
    }

    protected function _getMusicFiles(string $pathToScan): array
    {
        $di = new DirectoryIterator($pathToScan);
        $files = [];
        foreach ($di as $file){

            if($file->isDot() || ($file->isDir() && ($file->getBasename() == 'new' || $file->getBasename() == 'invalid'))) continue;

            if($file->isDir()){
                $files = array_merge($files, $this->_getMusicFiles($file->getRealPath()));
            }

            if(in_array($file->getExtension(), $this->_validMusicExt)){
                echo "Get info for file {$file->getRealPath()}\n";

                $files[$file->getRealPath()] = $this->_getMusicInfo($file->getRealPath(), $file->getExtension());
            } else if(!$file->isDir()) {
                $this->_copy($file->getRealPath(),  $this->_path . DIRECTORY_SEPARATOR . 'invalid' . DIRECTORY_SEPARATOR . pathinfo($file->getRealPath(), PATHINFO_BASENAME));
            }
        }
        return $files;
    }


    protected function _getMusicInfo(string $path, string $ext): file_iface
    {
        switch ($ext){
            case 'mp3':
                return (new id3_reader($path))->getTrackInfo();
        }
    }

    protected function _copy($from, $to)
    {
        echo "Coping: {$from}...{$to}\n";
        copy($from, $to);
    }
}