<?php
include ('fs/scanner.php');
include ('id3/reader.php');
include ('id3/tag/factory.php');
include ('file/iface.php');
include ('file/audio.php');


function main(int $argc, array $argv): int
{
    if($argc < 2) die("Please set path to scan\n");

    if(is_dir($argv[1])) {
        $c = new fs_scanner($argv[1]);
        $c->run();
    } else {
        die('Path not acceptable');
    }

    return 0;
}


main($argc, $argv);