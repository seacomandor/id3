<?php
class file_audio implements file_iface
{
    /** @var  string */
    protected $_trackName;
    /** @var  string */
    protected $_trackNo;
    /** @var  string */
    protected $_artist;
    /** @var  string */
    protected $_album;
    /** @var  int|string */
    protected $_year;

    /**
     * @param string $trackName
     */
    public function setTrackName(string $trackName)
    {
        if(!isset($this->_trackName)) {
            $this->_trackName = $trackName;
        }
    }

    /**
     * @param string $trackNo
     */
    public function setTrackNo(string $trackNo)
    {
        if(!isset($this->_trackNo)) {
            $trackNo = explode('/', $trackNo);
            if(strlen($trackNo[0]) == 1) $trackNo[0] = "0{$trackNo[0]}";

            $this->_trackNo = $trackNo[0];
        }
    }

    /**
     * @param string $artist
     */
    public function setArtist(string $artist)
    {
        if(!isset($this->_artist)) {
            $this->_artist = ucfirst(strtolower($artist));
        }
    }

    /**
     * @param string $album
     */
    public function setAlbum(string $album)
    {
        if(!isset($this->_artist)) {
            $this->_album = ucfirst(strtolower($album));
        }
    }

    /**
     * @param int|string $year
     */
    public function setYear(string $year)
    {
        if(!isset($this->_year)) {
            $this->_year = $year;
        }
    }


    public function getNewPath(string $extension): array
    {
        if($this->_isInfoNotValid()){
            throw new RuntimeException('file is unvalid');
        }

        return  [
            'dir' => $this->getDir(),
            'file' => $this->getFileName($extension),
        ];
    }

    public function getDir(): string
    {
        if($this->_isInfoNotValid()){
            throw new RuntimeException('file is unvalid');
        }
        return "new" . DIRECTORY_SEPARATOR .
                $this->_artist . DIRECTORY_SEPARATOR .
                $this->_year . ' - ' . $this->_album . DIRECTORY_SEPARATOR;
    }

    public function getFileName(string $extension):string
    {
        if($this->_isInfoNotValid()){
            throw new RuntimeException('file is unvalid');
        }
        $prefix = isset($this->_trackNo) ? $this->_trackNo . " - "  : '';
        return $prefix  . $this->_trackName . '.' . $extension;
    }

    protected function _isInfoNotValid(): bool
    {
        return $this->_artist    == null ||
               $this->_album     == null ||
               $this->_year      == null ||
               $this->_trackName == null;
    }
}