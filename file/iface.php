<?php
interface file_iface
{
    public function getNewPath(string $extension): array;
    public function getDir(): string;
    public function getFileName(string $extension): string;
}