<?php
class id3_reader
{
    /** @var  string path to file */
    protected $_filePath;
    /** @var  resource file descriptor */
    protected $_file;

    public function __construct(string $filePath)
    {
        $this->_filePath = $filePath;
    }
    
    public function getTrackInfo()
    {
        $this->_file = fopen($this->_filePath, 'rb');
        $version = $this->getId3Version();
        fclose($this->_file);

        if($version == 'v2_2'){
            var_dump($this->_filePath);
            $tagInfo = id3_tag_factory::getReader($version, $this->_filePath);
            var_dump($tagInfo->getTagInfo());
            die();
        }

        $tagInfo = id3_tag_factory::getReader($version, $this->_filePath);
        return $tagInfo->getTagInfo();
    }

    protected function getId3Version()
    {
        rewind($this->_file);
        $id3Header = fread($this->_file, 10);

        $format = 'a3TAG/H2version/H2subversion/H2flags/H8tagLength';
        $id3HeaderData = unpack($format, $id3Header);

        if($id3HeaderData['TAG'] == 'ID3'){
            $v = (int)$id3HeaderData['version'];
            return "v2_{$v}";
        } else {
            rewind($this->_file);
            fseek($this->_file, -128, SEEK_END);

            $id3Header = fread($this->_file, 128);
            $id3HeaderData = unpack('a3TAG/H242Other/H1End/H1trackNo/H1Genre', $id3Header);
            if($id3HeaderData['TAG'] == 'TAG'){
                if(hexdec($id3HeaderData['End']) == 0 && hexdec($id3HeaderData['trackNo']) != 0){
                    return "v1_1";
                } else {
                    return "v1_0";
                }
            }
        }
    }
}