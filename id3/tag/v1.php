<?php
abstract class id3_tag_v1 implements id3_tag_iface
{
    protected $_unpackPattern;
    protected $_fileDescriptor;

    public function __construct(string $path)
    {
        $this->_fileDescriptor = fopen($path, 'rb');
    }

    public function __destruct()
    {
        fclose($this->_fileDescriptor);
    }

    public function getTagInfo(): file_audio
    {
        rewind($this->_fileDescriptor);
        fseek($this->_fileDescriptor, -125, SEEK_END);
        $id3Bytes = fread($this->_fileDescriptor, 125);

        return $this->_unpack($id3Bytes);
    }

    abstract protected function _unpack(string $id3Bytes): file_audio;
}

class id3_tag_v1_0 extends id3_tag_v1
{
    public function __construct(string $path)
    {
        $this->_unpackPattern = 'a30TrackName/a30Artist/a30Album/a4Year/a30Comment/H2Genre';
        parent::__construct($path);
    }

    protected function _unpack(string $id3Bytes): file_audio
    {
        $id3data = unpack($this->_unpackPattern, $id3Bytes);

        $info = new file_audio();
        foreach ($id3data as $k => $v){
            $method = "set{$v}";
            if(method_exists($info, $method)){
                $info->$method($v);
            }
        }
        return $info;
    }
}

class id3_tag_v1_1 extends id3_tag_v1
{
    public function __construct(string $path)
    {
        $this->_unpackPattern = 'a30TrackName/a30Artist/a30Album/a4Year/a29comment/H2trackNo/H2genre';
        parent::__construct($path);
    }

    protected function _unpack(string $id3Bytes): file_audio
    {
        $id3data = unpack($this->_unpackPattern, $id3Bytes);

        if(isset($id3data['trackNo'])) $id3data['trackNo'] = hexdec($id3data['trackNo']);

        $info = new file_audio();
        foreach ($id3data as $k => $v){
            $method = "set{$v}";
            if(method_exists($info, $method)){
                $info->$method($v);
            }
        }
        return $info;
    }
}