<?php
abstract class id3_tag_v2 implements id3_tag_iface
{
    protected $_neededTags = [];
    protected $_tags2Values;
    protected $_tagStart;
    protected $_frameFormat;
    protected $_fileDescriptor;

    const LATIN1  = 0;
    const UTF16   = 1;
    const UTF16BE = 2;
    const UTF8    = 3;


    public function __construct($path)
    {
        $this->_setNeededTags();
        $this->_fileDescriptor = fopen($path, 'rb');
    }

    public function __destruct()
    {
        fclose($this->_fileDescriptor);
    }

    public function getTagInfo(): file_audio
    {
        return $this->_parseFile();
    }

    abstract protected function _setNeededTags();
    abstract protected function _getDataFromTag(int $length) : string;

    protected function _parseFile(): file_audio
    {
        $tagLength = $this->_getId3Length();

        rewind($this->_fileDescriptor);
        fseek($this->_fileDescriptor, 10, SEEK_CUR);
        $values = [];

        while(ftell($this->_fileDescriptor) < ($tagLength - 10) && !feof($this->_fileDescriptor)){
            $str = fread($this->_fileDescriptor, 10);
            $tagHeader = @unpack($this->_frameFormat, $str);
            if($tagHeader == false){
//                var_dump(ftell($this->_fileDescriptor));
//                fseek($this->_fileDescriptor, -10, SEEK_CUR);
//                var_dump($this->_frameFormat,$str, ftell($this->_fileDescriptor), $tagLength, $values, fread($this->_fileDescriptor, 10));
                break;
            }
            if(in_array($tagHeader['TAGID'], array_keys($this->_neededTags))){
                $values[$tagHeader['TAGID']] = $this->_getDataFromTag(hexdec($tagHeader['Length']));
            } else {
                fseek($this->_fileDescriptor, hexdec($tagHeader['Length']), SEEK_CUR);
            }
        }

        return $this->_getId3InfoEntity($values);
    }

    protected function _getId3InfoEntity(array $values): file_audio
    {
        $entity = new file_audio();
        foreach ($values as $k => $value){
            $method = "set{$this->_neededTags[$k]}";
            $entity->$method($value);
        }
        return $entity;
    }

    protected function _convertEncoding(string $str, int $encoding = self::UTF8)
    {
        switch ($encoding){
            case self::LATIN1:
                return mb_convert_encoding($str, 'UTF-8', 'Latin1');
            case self::UTF16:
            case self::UTF16BE:
                return mb_convert_encoding($str, 'UTF-8', 'UTF-16');
            default:
                return $str;
        }
    }

    protected function _getId3Length(): int
    {
        rewind($this->_fileDescriptor);

        $id3HeaderFormat = 'a3TAG/H2Version/H2Subversion/H2Flags/H8TagLength';
        $id3Header = unpack($id3HeaderFormat, fread($this->_fileDescriptor, 10));

        return hexdec($id3Header['TagLength']);
    }
}

/**  спользуется трёхсимвольный идентификатор фрейма вместо четырёхсимвольного («TT2» для фрейма с названием произведения вместо «TIT2»). */
class id3_tag_v2_2 extends id3_tag_v2
{
    protected function _setNeededTags()
    {
        $this->_neededTags = [
            'TAL' => 'Album',
            'TYE' => 'Year',
            'TOA' => 'Artist',
            'TP1' => 'Artist',
            'TRK' => 'TrackNo',
            'TT1' => 'TrackName',
            'TT2' => 'TrackName',
            'TT3' => 'TrackName',
        ];

        $this->_tagStart = 0;
        $this->_frameFormat = 'a3TAGID/H8Length/H4flags';
    }

    protected function _getDataFromTag(int $length): string
    {
        return '';
    }
}

class id3_tag_v2_3 extends id3_tag_v2
{
    protected function _setNeededTags()
    {
        $this->_neededTags = [
            'TALB' => 'Album',
            'TYER' => 'Year',
            'TOPE' => 'Artist',
            'TPE1' => 'Artist',
            'TRCK' => 'TrackNo',
            'TIT1' => 'TrackName',
            'TIT2' => 'TrackName',
            'TIT3' => 'TrackName',
        ];

        $this->_tagStart = 0;
        $this->_frameFormat = 'a4TAGID/H8Length/H4flags';
    }

    protected function _getDataFromTag(int $length): string
    {
        $txtLength = $length - 1;
        $format = "H2Encoding/a{$txtLength}Data";

        $tag = unpack($format, fread($this->_fileDescriptor, $length));

        $str =  $this->_convertEncoding($tag['Data'], hexdec($tag['Encoding']));
        return $str;
    }
}

/** Тэг может быть в конце(!) файла */
class id3_tag_v2_4 extends id3_tag_v2
{
    protected function _setNeededTags()
    {
        $this->_neededTags = [
            'TALB' => 'Album',
            'TYER' => 'Year',
            'TDRC' => 'Year',
            'TOPE' => 'Artist',
            'TPE1' => 'Artist',
            'TRCK' => 'TrackNo',
            'TIT1' => 'TrackName',
            'TIT2' => 'TrackName',
            'TIT3' => 'TrackName',
        ];
        $this->_tagStart = INF;
        $this->_frameFormat = 'a4TAGID/H8Length/H4flags';
    }

    protected function _getDataFromTag(int $length): string
    {
        $txtLength = $length - 1;
        $format = "H2Encoding/a{$txtLength}Data";

        $tag = unpack($format, fread($this->_fileDescriptor, $length));

        $str =  $this->_convertEncoding($tag['Data'], hexdec($tag['Encoding']));
        return $str;
    }
}