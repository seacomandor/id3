<?php
require_once ('iface.php');
require_once ('v1.php');
require_once ('v2.php');

class id3_tag_factory
{
    public static function getReader($tagVersion, $fd): id3_tag_iface
    {
        $className = "id3_tag_{$tagVersion}";
        if(class_exists($className)) {
            return new $className($fd);
        } else {
            var_dump($fd, $className);
            die();
        }
    }
}